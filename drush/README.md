## Per repository configuration for drush

Taken from [this blogpost][1] you can add this code to your ~/.drush/drushrc.php
file in order to have your options, commands and aliases loaded per repository

    $output = array();
    exec('git rev-parse --show-toplevel 2> /dev/null', $output);

    if (!empty($output)) {
      $repo = $output[0];

      $options['config'] = $repo . '/drush/drushrc.php';
      $options['include'] = $repo . '/drush/commands';
      $options['alias-path'] = $repo . '/drush/aliases';
    }


[1]: http://grayside.org/2011/08/configure-drush-your-git-repository

